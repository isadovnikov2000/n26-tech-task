### What is this repository for? ###

The `coding-challenge` artifact is the completed service for the technical N26 challenge

The technical challenge requirements can be found in [N26_Senior_Backend_Engineer_Coding_Challenge.pdf](N26_Senior_Backend_Engineer_Coding_Challenge.pdf)
### How do I get set up? ###
**System requirements**
* Java 8
* Maven 3+

**Build**
* `mvn clean compile`

**Tests**
* Unit tests `mvn clean test`
* Integration tests `mvn clean verify`

**Run**
* Standalone `mvn clean spring-boot:run`

### CI/CD ###

[Bitbucket Pipelines](bitbucket-pipelines.yml) are responsible for Continues Integration

Continues Delivery wasn't implemented

### Api ###

The api description is available at `http://service-host:service-port/swagger-ui/`
