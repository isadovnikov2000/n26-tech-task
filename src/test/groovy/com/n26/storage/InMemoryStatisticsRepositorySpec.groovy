package com.n26.storage

import com.n26.properties.StatisticsStorageProperties
import com.n26.services.dtos.Statistic
import com.n26.services.dtos.Transaction
import com.n26.utils.TimeManagerService
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import java.time.Duration
import java.time.Instant

class InMemoryStatisticsRepositorySpec extends Specification {

    private static def NOW = Instant.now()

    def timeManagerService = Mock(TimeManagerService)
    @Shared
    def properties = Mock(StatisticsStorageProperties)

    def storage = new InMemoryStatisticsRepository(timeManagerService, properties)

    def setupSpec() {
        properties.getBucketTimeDuration() >> Duration.ofSeconds(1)
        properties.getHistoryDuration() >> Duration.ofSeconds(60)
    }

    def cleanup() {
        storage.deleteAll()
    }

    @Unroll
    def "given transaction list when transactions saved then correct statistics is expected"() {
        given: "transactions"
        def transactions = transactionsMap(transactionKey)

        and: "current time is now"
        timeManagerService.getCurrentTime() >> NOW

        and: "expected statistics"
        expectedStatistics

        when: "all transactions saved"
        transactions.forEach {
            storage.add(it)
        }

        then: "statistics is correct"
        def actualStatistics = storage.getStatistics()
        expectedStatistics == actualStatistics

        where:
        transactionKey        || expectedStatistics
        TransactionsKey.ZERO  || new Statistic(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, 0L)
        TransactionsKey.ONE   || new Statistic(BigDecimal.ONE, BigDecimal.valueOf(1.00).setScale(2), BigDecimal.ONE, BigDecimal.ONE, 1L)
        TransactionsKey.TWO   || new Statistic(BigDecimal.valueOf(11), BigDecimal.valueOf(5.50).setScale(2), BigDecimal.TEN, BigDecimal.ONE, 2L)
        TransactionsKey.THREE || new Statistic(BigDecimal.valueOf(-1), BigDecimal.valueOf(-0.33), BigDecimal.valueOf(9223372036854775807L), BigDecimal.valueOf(-9223372036854775808L), 3L)
        TransactionsKey.TEN   || new Statistic(BigDecimal.valueOf(22), BigDecimal.valueOf(2.20).setScale(2), BigDecimal.valueOf(20130201L), BigDecimal.valueOf(-20130201L), 10L)
    }

    def "given statistics storage with transactions when deleteAll then statistics is empty"() {
        given: "transactions"
        def transactions = [
                new Transaction(BigDecimal.ONE, NOW),
                new Transaction(BigDecimal.TEN, NOW),
                new Transaction(BigDecimal.ZERO, NOW),
        ]

        and: "current time is now"
        timeManagerService.getCurrentTime() >> NOW

        and: "transactions saved to storage"
        transactions.forEach {
            storage.add(it)
        }
        and: "expected statistics"
        def expectedStatistics = new Statistic(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, 0L)

        when: "deleteAll executed"
        storage.deleteAll()

        then: "statistics is empty"
        def actualStatistics = storage.getStatistics()
        expectedStatistics == actualStatistics
    }

    private static Set<Transaction> transactionsMap(TransactionsKey key) {
        return [
                (TransactionsKey.ZERO) : [],
                (TransactionsKey.ONE)  : [
                        new Transaction(BigDecimal.ONE, NOW)
                ],
                (TransactionsKey.TWO)  : [
                        new Transaction(BigDecimal.ONE, NOW),
                        new Transaction(BigDecimal.TEN, NOW)
                ],
                (TransactionsKey.THREE): [
                        new Transaction(BigDecimal.valueOf(Long.MAX_VALUE), NOW),
                        new Transaction(BigDecimal.ZERO, NOW),
                        new Transaction(BigDecimal.valueOf(Long.MIN_VALUE), NOW),
                ],
                (TransactionsKey.TEN)  : [
                        new Transaction(BigDecimal.valueOf(19940603L), NOW),
                        new Transaction(BigDecimal.valueOf(-19940603L), NOW),
                        new Transaction(BigDecimal.valueOf(20130201L), NOW),
                        new Transaction(BigDecimal.valueOf(-20130201L), NOW),
                        new Transaction(BigDecimal.ZERO, NOW),
                        new Transaction(BigDecimal.TEN, NOW),
                        new Transaction(BigDecimal.ONE, NOW),
                        new Transaction(BigDecimal.ZERO, NOW.minusMillis(10)),
                        new Transaction(BigDecimal.TEN, NOW.minusMillis(10)),
                        new Transaction(BigDecimal.ONE, NOW.minusMillis(10))
                ]
        ][key]
    }

    private enum TransactionsKey {
        ZERO, ONE, TWO, THREE, TEN
    }
}
