package com.n26.storage

import com.n26.services.dtos.Statistic
import spock.lang.Specification
import spock.lang.Unroll


class StatisticsBucketSummarySpec extends Specification {

    def summary = new StatisticsBucketSummary()

    def cleanup() {
        summary.reset()
    }

    @Unroll
    def "given amounts list when amounts accepted then correct summary is expected"() {
        given: "amounts"
        def amounts = amountsMap(amountKey)

        and: "expected statistics"
        expectedSummary

        when: "all amounts saved"
        amounts.forEach {
            summary.accept(it)
        }

        then: "summary is correct"
        def actualSummary = summary.getStatistics()
        expectedSummary == actualSummary

        where:
        amountKey        || expectedSummary
        AmountsKey.ZERO  || new Statistic(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, 0L)
        AmountsKey.ONE   || new Statistic(BigDecimal.ONE, BigDecimal.valueOf(1.00).setScale(2), BigDecimal.ONE, BigDecimal.ONE, 1L)
        AmountsKey.TWO   || new Statistic(BigDecimal.valueOf(11), BigDecimal.valueOf(5.50).setScale(2), BigDecimal.TEN, BigDecimal.ONE, 2L)
        AmountsKey.THREE || new Statistic(BigDecimal.valueOf(-1), BigDecimal.valueOf(-0.33), BigDecimal.valueOf(9223372036854775807L), BigDecimal.valueOf(-9223372036854775808L), 3L)
        AmountsKey.TEN   || new Statistic(BigDecimal.valueOf(22), BigDecimal.valueOf(2.20).setScale(2), BigDecimal.valueOf(20130201L), BigDecimal.valueOf(-20130201L), 10L)
    }

    def "given summary storage with different amounts when reset then summary is empty"() {
        given: "amounts"
        def amounts = [
                BigDecimal.ONE,
                BigDecimal.TEN,
                BigDecimal.ZERO
        ]
        and: "amounts saved to summary"
        amounts.forEach {
            summary.accept(it)
        }
        and: "expected statistics"
        def expectedStatistics = new Statistic(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, 0L)

        when: "reset executed"
        summary.reset()

        then: "statistics is empty"
        def actualStatistics = summary.getStatistics()
        expectedStatistics == actualStatistics
    }

    private static List<BigDecimal> amountsMap(AmountsKey key) {
        return [
                (AmountsKey.ZERO) : [],
                (AmountsKey.ONE)  : [
                        BigDecimal.ONE
                ],
                (AmountsKey.TWO)  : [
                        BigDecimal.ONE,
                        BigDecimal.TEN
                ],
                (AmountsKey.THREE): [
                        BigDecimal.valueOf(Long.MAX_VALUE),
                        BigDecimal.ZERO,
                        BigDecimal.valueOf(Long.MIN_VALUE),
                ],
                (AmountsKey.TEN)  : [
                        BigDecimal.valueOf(19940603L),
                        BigDecimal.valueOf(-19940603L),
                        BigDecimal.valueOf(20130201L),
                        BigDecimal.valueOf(-20130201L),
                        BigDecimal.ZERO,
                        BigDecimal.TEN,
                        BigDecimal.ONE,
                        BigDecimal.ZERO,
                        BigDecimal.TEN,
                        BigDecimal.ONE
                ]
        ][key]
    }

    private enum AmountsKey {
        ZERO, ONE, TWO, THREE, TEN
    }
}
