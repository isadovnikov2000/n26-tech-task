package com.n26.controllers

import com.n26.AbstractIntegrationSpec
import groovy.json.JsonBuilder
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import spock.lang.Unroll

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class TransactionControllerIntegrationSpec extends AbstractIntegrationSpec {

    @Unroll
    def "when transaction[#amount, #timestamp] is submitted then #expectedHttpCode code returned"() {
        given: 'transaction body'
        def transaction = [
                "amount"   : amount,
                "timestamp": timestamp instanceof Instant ? DateTimeFormatter.ISO_INSTANT.format(timestamp) : timestamp
        ]

        when: 'create transaction requested'
        def resultActions = mvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new JsonBuilder(transaction).toString()))

        then: 'api status code is 200'
        resultActions.andExpect(status().is(expectedHttpCode.value()))

        where:
        amount    | timestamp                                                  || expectedHttpCode
        "12.3343" | Instant.now()                                              || HttpStatus.CREATED
        "12.3343" | LocalDateTime.now().minusDays(6).toInstant(ZoneOffset.UTC) || HttpStatus.NO_CONTENT
        "12.3343" | LocalDateTime.now().plusDays(6).toInstant(ZoneOffset.UTC)  || HttpStatus.UNPROCESSABLE_ENTITY
        "n26"     | Instant.now()                                              || HttpStatus.UNPROCESSABLE_ENTITY
        "12.3343" | "n26"                                                      || HttpStatus.UNPROCESSABLE_ENTITY
        "12.3343" | null                                                       || HttpStatus.UNPROCESSABLE_ENTITY
        null      | Instant.now()                                              || HttpStatus.UNPROCESSABLE_ENTITY
    }


    def "when json body is has invalid format then 400 code returned"() {
        given: 'invalid format json'
        def jsonBody = "Why I was not able to create an account in N26?"

        when: 'create transaction requested'
        def resultActions = mvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))

        then: 'api status code is 400'
        resultActions.andExpect(status().isBadRequest())
    }

    def "when delete all transaction requested then 204 code returned"() {
        when: 'create transaction requested'
        def resultActions = mvc.perform(delete("/transactions"))

        then: 'api status code is 204'
        resultActions.andExpect(status().isNoContent())
    }
}
