package com.n26.controllers

import com.n26.AbstractIntegrationSpec
import groovy.json.JsonSlurper
import org.springframework.test.annotation.DirtiesContext

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
class StatisticControllerIntegrationSpec extends AbstractIntegrationSpec {

    private def jsonSlurper = new JsonSlurper()

    def "when statics request submitted then expected result returned"() {
        when: 'statistics requested'
        def resultActions = mvc.perform(get("/statistics"))

        then: 'api status code is 200'
        resultActions.andExpect(status().isOk())

        and: 'return content is not null'
        def responseContent = jsonSlurper.parseText(resultActions.andReturn().response.contentAsString)


        and: 'the response content is complete'
        responseContent.sum == "0.00"
        responseContent.avg == "0.00"
        responseContent.sum == "0.00"
        responseContent.min == "0.00"
        responseContent.count == 0
    }

}
