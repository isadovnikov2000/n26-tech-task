package com.n26.services

import com.n26.exceptions.ExpiredTransactionException
import com.n26.exceptions.TransactionInFutureException
import com.n26.properties.TransactionValidationProperties
import com.n26.services.dtos.Transaction
import com.n26.utils.TimeManagerService
import spock.lang.Specification
import spock.lang.Unroll

import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit

class DefaultTransactionServiceSpec extends Specification {

    private static Instant NOW = Instant.now()

    def  statisticService = Mock(StatisticService)
    def timeManagerService = Mock(TimeManagerService)
    def properties = Mock(TransactionValidationProperties)

    def transactionService = new DefaultTransactionService(statisticService, timeManagerService, properties)

    @Unroll
    def "given invalid transaction[#amount, #timestamp] when saved then exception[#expectedException] thrown"() {
        given: "invalid transaction"
        def transaction = new Transaction(amount, timestamp)

        and: "current time is now"
        timeManagerService.getCurrentTime() >> NOW

        and: "and transaction expired duration"
        properties.getExpirationDuration() >> Duration.ofSeconds(60)

        when: "trying to save invalid transaction"
        transactionService.save(transaction)

        then: "exception thrown"
        thrown(expectedException)

        where:
        amount         | timestamp                         || expectedException
        BigDecimal.ONE | NOW.minus(6, ChronoUnit.DAYS)     || ExpiredTransactionException
        BigDecimal.ONE | NOW.plus(6, ChronoUnit.DAYS)      || TransactionInFutureException
        BigDecimal.ONE | NOW.minus(61, ChronoUnit.SECONDS) || ExpiredTransactionException
        BigDecimal.ONE | NOW.plus(60, ChronoUnit.SECONDS)  || TransactionInFutureException
    }

    @Unroll
    def "given valid transaction[#amount, #timestamp] when saved then nothing"() {
        given: "valid transaction"
        def transaction = new Transaction(amount, timestamp)

        and: "current time is now"
        timeManagerService.getCurrentTime() >> NOW

        and: "and transaction expired duration"
        properties.getExpirationDuration() >> Duration.ofSeconds(60)

        when: "trying to save valid transaction"
        transactionService.save(transaction)

        then: "statistics service called ones"
        1 * statisticService.add(transaction)

        where:
        amount         | timestamp
        BigDecimal.ONE | NOW
        BigDecimal.ONE | NOW.minus(10, ChronoUnit.SECONDS)
        BigDecimal.ONE | NOW.minus(60, ChronoUnit.SECONDS)
    }
}
