package com.n26

import com.n26.controllers.StatisticController
import com.n26.controllers.TransactionController
import org.springframework.beans.factory.annotation.Autowired

class LoadContextIntegrationSpec extends AbstractIntegrationSpec {

    @Autowired
    public StatisticController statisticsController

    @Autowired
    public TransactionController transactionsController

    def "when context is loaded then all expected beans are created"() {
        expect: "the Controllers are created"
        statisticsController
        transactionsController
    }
}
