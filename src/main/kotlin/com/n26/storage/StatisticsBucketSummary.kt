package com.n26.storage

import com.n26.services.dtos.Statistic
import java.math.BigDecimal
import java.util.function.Consumer

class StatisticsBucketSummary : Consumer<BigDecimal> {

    private var count: Long
    private var sum: BigDecimal
    private var max: BigDecimal
    private var min: BigDecimal
    private var avg: BigDecimal

    init {
        count = 0L
        sum = BigDecimal.ZERO
        max = MIN_VALUE
        min = MAX_VALUE
        avg = BigDecimal.ZERO
    }

    override fun accept(bigDecimal: BigDecimal) {
        max = maximum(max, bigDecimal)
        min = minimum(min, bigDecimal)
        sum = sum.add(bigDecimal)
        count = ++count
    }

    fun getStatistics(): Statistic = Statistic(
        sum,
        getAvg(),
        getMax(),
        getMin(),
        getCount()
    )

    fun reset() {
        count = 0L
        sum = BigDecimal.ZERO
        max = MIN_VALUE
        min = MAX_VALUE
        avg = BigDecimal.ZERO
    }

    fun merge(statistics: StatisticsBucketSummary): StatisticsBucketSummary {
        count += statistics.count
        sum = sum.add(statistics.sum)
        max = maximum(max, statistics.max)
        min = minimum(min, statistics.min)
        return this
    }

    private fun getCount(): Long {
        return count
    }

    private fun getMax(): BigDecimal {
        return if (max == MIN_VALUE) {
            BigDecimal.ZERO
        } else max
    }

    private fun getMin(): BigDecimal {
        return if (min == MAX_VALUE) {
            BigDecimal.ZERO
        } else min
    }

    private fun getAvg(): BigDecimal {
        return if (count == 0L) {
            BigDecimal.ZERO
        } else {
            sum.divide(BigDecimal(count), 2, BigDecimal.ROUND_HALF_UP)
        }
    }

    private fun maximum(max: BigDecimal, input: BigDecimal): BigDecimal {
        return if (input > max) {
            input
        } else {
            max
        }
    }

    private fun minimum(min: BigDecimal, input: BigDecimal): BigDecimal {
        return if (input < min) {
            input
        } else {
            min
        }
    }

    companion object {
        private val MAX_VALUE = BigDecimal.valueOf(Long.MAX_VALUE)
        private val MIN_VALUE = BigDecimal.valueOf(Long.MIN_VALUE)
    }
}
