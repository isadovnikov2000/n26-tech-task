package com.n26.storage

import com.n26.services.dtos.Statistic
import java.math.BigDecimal
import java.time.Instant

class StatisticsBucket(
    val timestamp: Instant
) {
    private var statistics = StatisticsBucketSummary()

    fun add(amount: BigDecimal) {
        statistics.accept(amount)
    }

    fun merge(statisticsBucket: StatisticsBucket): StatisticsBucket {
        statistics = statistics.merge(statisticsBucket.statistics)
        return this
    }

    fun reset() {
        statistics.reset()
    }

    fun getStatistics(): Statistic = statistics.getStatistics()
}
