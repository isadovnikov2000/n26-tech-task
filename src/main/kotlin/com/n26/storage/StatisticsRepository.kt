package com.n26.storage

import com.n26.properties.StatisticsStorageProperties
import com.n26.services.dtos.Statistic
import com.n26.services.dtos.Transaction
import com.n26.utils.TimeManagerService
import org.springframework.stereotype.Repository
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

interface StatisticsRepository {
    fun add(transaction: Transaction)
    fun getStatistics(): Statistic
    fun deleteAll()
}

@Repository
class InMemoryStatisticsRepository(
    private val timeManagerService: TimeManagerService,
    properties: StatisticsStorageProperties
) : StatisticsRepository {

    private val ONE_BUCKET_TIME_DURATION_MILLIS = properties.bucketTimeDuration.toMillis()
    private val STORAGE_HISTORY_DURATION_MILLIS = properties.historyDuration.toMillis()
    private val BUCKETS_COUNT = (STORAGE_HISTORY_DURATION_MILLIS / ONE_BUCKET_TIME_DURATION_MILLIS).toInt()

    private var buckets: Array<StatisticsBucket?> = arrayOfNulls(BUCKETS_COUNT)
    private val locks: Array<ReentrantLock?> = arrayOfNulls(BUCKETS_COUNT)
    private var cachedStatistics: StatisticsBucket = StatisticsBucket(Instant.MIN.truncatedTo(ChronoUnit.SECONDS))

    private val currentTime: Instant
        get() {
            return timeManagerService.getCurrentTime()
        }

    override fun add(transaction: Transaction) {
        val bucketIndex = getBucketIndexFor(transaction.timestamp)
        lock(bucketIndex)
        try {
            val bucket: StatisticsBucket = getBucketFor(transaction.timestamp, bucketIndex)
            bucket.add(transaction.amount)
        } finally {
            unlock(bucketIndex)
        }
    }

    override fun getStatistics(): Statistic {
        if (cachedStatistics.timestamp.isBefore(currentTime)) {
            cachedStatistics = generateStatistics()
        }
        return cachedStatistics.getStatistics()
    }

    override fun deleteAll() {
        buckets = arrayOfNulls(BUCKETS_COUNT)
    }

    private fun getBucketFor(timestamp: Instant, bucketIndex: Int): StatisticsBucket {
        val bucket: StatisticsBucket = getStatisticsBucketOrDefault(timestamp, bucketIndex)
        resetIfOutdated(bucketIndex, bucket)
        return bucket
    }

    private fun generateStatistics(): StatisticsBucket {
        val total = StatisticsBucket(currentTime.truncatedTo(ChronoUnit.SECONDS))
        for (bucketIndex in buckets.indices) {
            val bucket: StatisticsBucket? = buckets[bucketIndex]
            if (bucket != null) {
                resetIfOutdated(bucketIndex, bucket)
                total.merge(bucket)
            }
        }
        return total
    }

    private fun resetIfOutdated(bucketIndex: Int, bucket: StatisticsBucket) {
        lock(bucketIndex)
        try {
            if (isOutdated(bucket.timestamp)) {
                bucket.reset()
            }
        } finally {
            unlock(bucketIndex)
        }
    }

    private fun getStatisticsBucketOrDefault(timestamp: Instant, bucketIndex: Int): StatisticsBucket {
        if (buckets[bucketIndex] == null) {
            buckets[bucketIndex] = StatisticsBucket(timestamp)
        }
        return buckets[bucketIndex]!!
    }

    private fun getLockOrDefault(bucketIndex: Int): Lock {
        if (locks[bucketIndex] == null) {
            locks[bucketIndex] = ReentrantLock()
        }
        return locks[bucketIndex]!!
    }

    private fun unlock(bucketIndex: Int) {
        getLockOrDefault(bucketIndex).unlock()
    }

    private fun lock(bucketIndex: Int) {
        getLockOrDefault(bucketIndex).lock()
    }

    private fun isOutdated(bucketTime: Instant): Boolean {
        return bucketTime.isBefore(currentTime.minusMillis(STORAGE_HISTORY_DURATION_MILLIS))
    }

    private fun getBucketIndexFor(timestamp: Instant): Int {
        return (truncateTimestamp(timestamp) % BUCKETS_COUNT).toInt()
    }

    private fun truncateTimestamp(timestamp: Instant): Long {
        return timestamp.toEpochMilli() / ONE_BUCKET_TIME_DURATION_MILLIS
    }
}
