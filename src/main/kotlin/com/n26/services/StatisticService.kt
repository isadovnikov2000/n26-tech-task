package com.n26.services

import com.n26.services.dtos.Statistic
import com.n26.services.dtos.Transaction
import com.n26.storage.StatisticsRepository
import org.springframework.stereotype.Service

interface StatisticService {
    fun getStatistics(): Statistic
    fun add(transaction: Transaction)
    fun deleteAll()
}

@Service
class DefaultStatisticService(
    private val repository: StatisticsRepository
) : StatisticService {

    override fun getStatistics(): Statistic = repository.getStatistics()

    override fun add(transaction: Transaction) {
        repository.add(transaction)
    }

    override fun deleteAll() {
        repository.deleteAll()
    }

}
