package com.n26.services

import com.n26.exceptions.ExpiredTransactionException
import com.n26.exceptions.TransactionInFutureException
import com.n26.properties.TransactionValidationProperties
import com.n26.services.dtos.Transaction
import com.n26.utils.TimeManagerService
import mu.KotlinLogging
import org.springframework.stereotype.Service

private val logger = KotlinLogging.logger {}

interface TransactionService {
    fun save(transaction: Transaction)
    fun deleteAll()
}

@Service
class DefaultTransactionService(
    private val statisticService: StatisticService,
    private val timeManagerService: TimeManagerService,
    private val properties: TransactionValidationProperties
) : TransactionService {

    override fun save(transaction: Transaction) {
        validateTransaction(transaction)
        statisticService.add(transaction)
    }

    override fun deleteAll() {
        statisticService.deleteAll()
    }

    private fun validateTransaction(transaction: Transaction) = with(transaction) {
        val currentTimestamp = timeManagerService.getCurrentTime()
        val validTimestamp = currentTimestamp.minus(properties.expirationDuration)
        when {
            currentTimestamp.isBefore(timestamp) -> {
                val errorMessage = "Transaction[$timestamp] is future. Current Time [$currentTimestamp]."
                logger.warn(errorMessage)
                throw TransactionInFutureException(errorMessage)
            }
            timestamp.isBefore(validTimestamp) -> {
                val errorMessage = "Transaction[$timestamp] is expired. Valid Time [$validTimestamp]."
                logger.warn(errorMessage)
                throw ExpiredTransactionException(errorMessage)
            }
            else -> {
                logger.trace { "Transaction[$amount, $timestamp] has been validated successfully" }
            }
        }
    }

}
