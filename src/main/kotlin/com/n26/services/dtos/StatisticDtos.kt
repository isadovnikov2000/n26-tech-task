package com.n26.services.dtos

import java.math.BigDecimal

data class Statistic(
    val sum: BigDecimal,
    val avg: BigDecimal,
    val max: BigDecimal,
    val min: BigDecimal,
    val count: Long
)
