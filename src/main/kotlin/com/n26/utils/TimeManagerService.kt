package com.n26.utils

import org.springframework.stereotype.Service
import java.time.Instant

interface TimeManagerService {
    fun getCurrentTime(): Instant
}

@Service
class DefaultTimeManagerService : TimeManagerService {
    override fun getCurrentTime(): Instant = Instant.now()
}
