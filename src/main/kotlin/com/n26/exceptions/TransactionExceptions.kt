package com.n26.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class InvalidFormalTransactionException(message: String, throwable: Throwable): RuntimeException(message, throwable)

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class TransactionInFutureException(message: String): RuntimeException(message)

@ResponseStatus(HttpStatus.NO_CONTENT)
class ExpiredTransactionException(message: String): RuntimeException(message)
