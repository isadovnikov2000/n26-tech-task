package com.n26.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import java.time.Duration

@ConfigurationProperties(prefix = "transaction.validation")
open class TransactionValidationProperties {
    open lateinit var expirationDuration: Duration
}
