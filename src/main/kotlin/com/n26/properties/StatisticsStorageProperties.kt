package com.n26.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import java.time.Duration

@ConfigurationProperties(prefix = "statistic.storage")
open class StatisticsStorageProperties {
    open lateinit var historyDuration: Duration
    open lateinit var bucketTimeDuration: Duration
}
