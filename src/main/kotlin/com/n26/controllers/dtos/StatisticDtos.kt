package com.n26.controllers.dtos

import io.swagger.annotations.ApiModelProperty

data class StatisticResponse(
    @ApiModelProperty(
        value = "a BigDecimal specifying the total sum of transaction value in the last 60 seconds",
        name = "sum",
        dataType = "String",
        example = "0.00")
    val sum: String,
    @ApiModelProperty(
        value = "a BigDecimal specifying the average amount of transaction value in the last 60 seconds",
        name = "avg",
        dataType = "String",
        example = "0.00")
    val avg: String,
    @ApiModelProperty(
        value = "a BigDecimal specifying single highest transaction value in the last 60 seconds",
        name = "max",
        dataType = "String",
        example = "0.00")
    val max: String,
    @ApiModelProperty(
        value = "a BigDecimal specifying single lowest transaction value in the last 60 seconds",
        name = "min",
        dataType = "String",
        example = "0.00")
    val min: String,
    @ApiModelProperty(
        value = "a long specifying the total number of transactions that happened in the last 60 seconds",
        name = "count",
        dataType = "String",
        example = "0")
    val count: Long
)
