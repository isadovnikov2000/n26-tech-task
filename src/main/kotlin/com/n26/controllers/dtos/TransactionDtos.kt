package com.n26.controllers.dtos

import io.swagger.annotations.ApiModelProperty

data class CreateTransactionRequest(
    @ApiModelProperty(
        value = "transaction amount; a string of arbitrary length that is parsable as a BigDecimal",
        name = "amount",
        dataType = "String",
        example = "12.3343") //TODO contribute to https://github.com/springfox/springfox/issues/3479
    val amount: String?,
    @ApiModelProperty(
        value = "transaction time in the ISO 8601 format \"YYYY-MM-DDThh:mm:ss.sssZ\" in the UTC timezone (this is not the current timestamp)",
        name = "timestamp",
        dataType = "String",
        example = "2018-07-17T09:59:51.312Z")
    val timestamp: String?
)
