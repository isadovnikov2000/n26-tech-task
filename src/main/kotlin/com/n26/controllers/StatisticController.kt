package com.n26.controllers

import com.n26.controllers.dtos.StatisticResponse
import com.n26.services.StatisticService
import com.n26.services.dtos.Statistic
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal.ROUND_HALF_UP

@RestController
@RequestMapping("/statistics")
class StatisticController(
    private val statisticService: StatisticService
) {

    @Operation(summary = "Get a statistic for last 60 seconds")
    @ApiResponse(responseCode = "200",
        description = "Found the statistic",
        content = [Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = Schema(implementation = StatisticResponse::class))]
    )
    @GetMapping
    fun getStatistics(): StatisticResponse = statisticService.getStatistics().toStatisticResponse()
}

fun Statistic.toStatisticResponse(): StatisticResponse = StatisticResponse(
    sum.setScale(2, ROUND_HALF_UP).toString(),
    avg.setScale(2, ROUND_HALF_UP).toString(),
    max.setScale(2, ROUND_HALF_UP).toString(),
    min.setScale(2, ROUND_HALF_UP).toString(),
    count
)
