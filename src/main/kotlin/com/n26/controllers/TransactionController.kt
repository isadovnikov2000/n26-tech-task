package com.n26.controllers

import com.n26.controllers.dtos.CreateTransactionRequest
import com.n26.exceptions.InvalidFormalTransactionException
import com.n26.services.TransactionService
import com.n26.services.dtos.Transaction
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.time.Instant

@RestController
@RequestMapping("/transactions")
class TransactionController(
    private val transactionService: TransactionService
) {

    @Operation(summary = "Create a transaction")
    @ApiResponses(value = [
        ApiResponse(responseCode = "201", description = "Transaction has been created successfully"),
        ApiResponse(responseCode = "204", description = "Transaction is older than 60 seconds"),
        ApiResponse(responseCode = "400", description = "Invalid JSON format"),
        ApiResponse(responseCode = "422", description = "Any of the fields are not parsable or the transaction date is in the future")])
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createTransaction(@RequestBody createTransactionRequest: CreateTransactionRequest) {
        val transaction = createTransactionRequest.toTransaction()
        transactionService.save(transaction)
    }

    @Operation(summary = "Delete all transactions")
    @ApiResponse(responseCode = "204", description = "Transactions have been deleted successfully")
    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteAllTransactions() {
        transactionService.deleteAll()
    }
}

fun CreateTransactionRequest.toTransaction(): Transaction {
    val timestamp = try {
        Instant.parse(timestamp)
    } catch (ex: Exception) {
        throw InvalidFormalTransactionException("Transaction timestamp has invalid format [$timestamp]", ex)
    }
    val amount = try {
        BigDecimal(amount)
    } catch (ex: Exception) {
        throw InvalidFormalTransactionException("Transaction amount has invalid format [$amount]", ex)
    }
    return Transaction(
        amount = amount,
        timestamp = timestamp
    )
}
